# Fasterize

## Pour commencer

Entrez ici les instructions pour bien débuter avec votre projet
First, clone the project
git clone <clone url>

Then

```bash
npm i
// to start API server you will have to access server package
cd packages/server
npm start

// to start API e2e tests
npm run test:e2e


// to start front react application you will have to access client package
cd packages/client
npm start

// to start front cypress e2e tests
npm run cypress

Cypress window launch, then click on "app.spec.js", it will run cypress tests
```
