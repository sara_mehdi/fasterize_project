import React, { useState } from "react";
import { Formik } from "formik";
import { Container, AppBar, Typography, Grid } from "@material-ui/core";

import { UrlForm } from "./UrlForm";
import { UrlFormSchema } from "./schema/urlFormSchema";
import UrlList from "./helpers/urlList";
import { useStyles } from "./useStyles";
import { URL_NOT_VALID, UNREACHABLE_URL } from "./helpers/errors";

const initialValues = {
  url: "https://",
};

const getCurrentDate = (separator = "/") => {
  let newDate = new Date();
  let date = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();

  return `${date}${separator}${
    month < 10 ? `0${month}` : `${month}`
  }${separator}${year}`;
};

export const UrlAnalysis = () => {
  const classes = useStyles();
  const [loader, setLoader] = useState(false);

  const handleSubmit = async (values, { setFieldError }) => {
    setLoader(true);

    const response = await fetch(`http://localhost:3001?url=${values.url}`, {
      "Content-Type": "application/json",
      method: "GET",
    });
    if (response.status === 200) {
      const body = await response.json();

      const storage =
        localStorage.length > 0 ? JSON.parse(localStorage.getItem("data")) : [];

      const row = {
        url: values.url,
        status: body.plugged,
        flags: body.fstrzFlags,
        cloudFrontStatus: body.cloudfrontStatus,
        cloudFrontPop: body.cloudfrontPOP,
        date: getCurrentDate(),
      };

      storage.push(row);
      localStorage.setItem("data", JSON.stringify(storage));

      setLoader(false);
    } else {
      if (response.status === 400) setFieldError("url", URL_NOT_VALID);
      else {
        setFieldError("url", UNREACHABLE_URL);
      }
    }
  };
  return (
    <Container className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Typography className={classes.title}>FASTERIZE DEBUGGER</Typography>
      </AppBar>
      <Grid container className={classes.container}>
        <Grid item>
          <div variant="square" className={classes.icon} />
        </Grid>
        <Grid item xs={10} className={classes.square}>
          HEADER DEBUGGER
        </Grid>
      </Grid>
      <Formik
        initialValues={initialValues}
        validationSchema={UrlFormSchema}
        initialErrors={initialValues}
        component={UrlForm}
        onSubmit={handleSubmit}
      />
      <Grid container className={classes.container}>
        <Grid item>
          <div variant="square" className={classes.icon} />
        </Grid>
        <Grid item xs={10} className={classes.square}>
          HISTORY
        </Grid>
      </Grid>
      <UrlList />
    </Container>
  );
};
