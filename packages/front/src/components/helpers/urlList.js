import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Chip } from "@material-ui/core";
import CloudIcon from "@material-ui/icons/Cloud";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const status = (row) => {
  if (row.status && row.flags.includes("optimisée"))
    return <CloudIcon style={{ color: "green" }} />;
  if (row.status && !row.flags.includes("optimisée"))
    return <CloudIcon style={{ color: "orange" }} />;
  if (!row.status) return <CloudIcon style={{ color: "red" }} />;
};

export default function UrlList() {
  const classes = useStyles();
  const rows = JSON.parse(localStorage.getItem("data"));

  return (
    <>
      {rows && (
        <TableContainer component={Paper} style={{ height: 350 }}>
          <Table className={classes.table} id="list">
            <TableHead style={{ backgroundColor: "#eeeeee" }}>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell align="center">URL</TableCell>
                <TableCell align="center">Status</TableCell>
                <TableCell align="center">Flags</TableCell>
                <TableCell align="center">CloudFront Status</TableCell>
                <TableCell align="center">CloudFront Pop</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.url}>
                  <TableCell component="th" scope="row">
                    {row.date}
                  </TableCell>
                  <TableCell align="center">{row.url}</TableCell>
                  <TableCell align="center">{status(row)}</TableCell>
                  {row.status && (
                    <>
                      <TableCell align="center">
                        {row.flags.map((el) => (
                          <Chip key={el} label={el} color="primary" />
                        ))}
                      </TableCell>
                      <TableCell align="center">
                        {row.cloudFrontStatus === "Miss from cloudfront" ? (
                          <Chip label="MISS" color="secondary" />
                        ) : (
                          <Chip label="HIT" color="primary" />
                        )}
                      </TableCell>
                      <TableCell align="center">{row.cloudFrontPop}</TableCell>
                    </>
                  )}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </>
  );
}
