export const URL_REQUIRED = "The url is required";
export const URL_NOT_VALID = "The url is not valid";
export const UNREACHABLE_URL = "The url is unreachable";
