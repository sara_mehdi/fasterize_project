import React from "react";
import TextField from "@material-ui/core/TextField";
import { Button, Grid } from "@material-ui/core";

import { useStyles } from "./useStyles";

export const UrlForm = ({
  handleSubmit,
  handleBlur,
  handleChange,
  values,
  errors,
  touched,
  isSubmitting,
  isValid,
}) => {
  const classes = useStyles();
  return (
    <form onSubmit={handleSubmit} className={classes.form}>
      <section>
        <Grid container spacing={1}>
          <Grid item xs={9}>
            <TextField
              autoFocus
              className={classes.textField}
              label="Url to check"
              id="url"
              name="url"
              variant="outlined"
              value={values.url}
              onChange={handleChange}
              onBlur={handleBlur}
              error={touched.url && errors.url !== undefined}
              helperText={touched.url && errors.url}
            />
          </Grid>
          <Grid item xs={3}>
            <Button
              id="button"
              className={classes.button}
              color="primary"
              variant="contained"
              type="submit"
              disabled={isSubmitting || !isValid}
            >
              LAUNCH ANALYSIS
            </Button>
          </Grid>
        </Grid>
      </section>
    </form>
  );
};
