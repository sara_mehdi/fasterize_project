import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  form: {
    alignContent: "center",
    width: "100%",
    "& > section": {
      display: "flex",
      alignContent: "center",
      justifyContent: "center",
      flexDirection: "column",
      position: "relative",
      backgroundColor: "#FFFFFF",
      boxShadow:
        "0 0 0 1px rgba(63,63,68,0.05), 0 1px 3px 0 rgba(63,63,68,0.15)",
      borderRadius: 4,
      marginBottom: 24,
      padding: "50px 30px 30px 30px",
    },
  },
  root: {
    backgroundColor: "#eeeeee",
    width: "150vh",
    height: "100vh",
  },
  title: {
    fontSize: "14px",
    color: "#0277bd",
    fontWeight: 700,
    textAlign: "center",
  },
  appBar: {
    height: "50px",
    marginBottom: "50px",
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#fafafa",
  },
  button: {
    height: "100%",
  },
  textField: {
    width: "100%",
  },
  square: {
    color: "#616161",
    fontSize: "12px",
    fontWeight: 800,
    marginTop: "7px",
  },
  container: {
    marginBottom: "20px",
  },
  icon: {
    marginTop: "2px",
    marginRight: "10px",
    justifyContent: "center",
    alignItems: "center",
    height: "25px",
    width: "25px",
    backgroundColor: "#4db6ac",
  },
});
