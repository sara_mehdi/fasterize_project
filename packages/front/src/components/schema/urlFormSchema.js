import * as Yup from "yup";
import { URL_REQUIRED, URL_NOT_VALID } from "../helpers/errors";

export const UrlFormSchema = Yup.object().shape({
  url: Yup.string().trim().required(URL_REQUIRED).url(URL_NOT_VALID, {
    message:
      "/^((https?)://)?(www.)?[a-z0-9]+(.[a-z]{2,}){1,3}(#?/?[a-zA-Z0-9#]+)*/?(?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/",
  }),
});
