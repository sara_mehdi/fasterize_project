import React from "react";
import { UrlAnalysis } from "./components/urlAnalysis";
import { Grid, makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  paper: {
    backgroundColor: "#283593",
    height: "100vh",
  },
});
function App() {
  const classes = useStyles();
  return (
    <div className="App">
      <Grid container>
        <Grid item xs={2}>
          <div className={classes.paper}></div>
        </Grid>
        <Grid item xs={9}>
          <UrlAnalysis />
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
