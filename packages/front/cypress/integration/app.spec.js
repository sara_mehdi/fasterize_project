/// <reference types="cypress" />

const {
  URL_NOT_VALID,
  UNREACHABLE_URL,
  URL_REQUIRED,
} = require("../../src/components/helpers/errors");

const goodURL = "nocibe.fr";
const invalidURL = "someUrl.c";
const unreachableURL = "facebook.ca";

describe("URL FORM E2E TESTING", () => {
  it("Should show url format error", () => {
    cy.visit("/");
    cy.get("input[name=url]")
      .click()
      .type(" ")
      .should("have.value", "https:// ")
      .blur()
      .get("p[id=url-helper-text]")
      .should("contain", URL_NOT_VALID);

    cy.get("button[id=button]").should("have.class", "Mui-disabled");
  });

  it("Should show required url error", () => {
    cy.visit("/");
    cy.get("input[id=url]")
      .click()
      .clear()
      .blur()
      .get("p[id=url-helper-text]")
      .should("contain", URL_REQUIRED);

    cy.get("button[id=button]").should("have.class", "Mui-disabled");
  });

  it("Should enable submit button", () => {
    cy.visit("/");

    cy.get("input[id=url]")
      .click()
      .type(goodURL)
      .blur()
      .get("button[id=button]");

    cy.get("button[id=button]").should("not.have.class", "Mui-disabled");
  });

  it("Should get OK status and add the result to url analysis list", () => {
    cy.visit("/");

    cy.get("input[id=url]").click().type(goodURL).blur();
    cy.get("button[id=button]")
      .should("not.have.class", "Mui-disabled")
      .click()
      .should(() => {
        expect(localStorage.length).to.greaterThan(0);
        expect(localStorage.getItem("data")).to.contain(`https://${goodURL}`);
      });

    cy.get("table[id=list]").should("not.be.null");
  });

  it("Should call Api and get bad request error", () => {
    cy.visit("/");

    cy.get("input[id=url]").click().type(invalidURL).blur();
    cy.get("button[id=button]")
      .should("not.have.class", "Mui-disabled")
      .click()
      .should(() => {
        expect(localStorage.getItem("data")).to.eq(null);
      });

    cy.get("table[id=list]").should("not.be.focused");
    cy.get("p[id=url-helper-text]").should("contain", URL_NOT_VALID);
  });

  it("Should call Api and get internal server error for unreachable url", () => {
    cy.visit("/");

    cy.get("input[id=url]").click().type(unreachableURL).blur();
    cy.get("button[id=button]")
      .should("not.have.class", "Mui-disabled")
      .click()
      .should(() => {
        expect(localStorage.getItem("data")).to.eq(null);
      });

    cy.get("table[id=list]").should("not.be.focused");
    cy.get("p[id=url-helper-text]").should("contain", UNREACHABLE_URL);
  });
});
