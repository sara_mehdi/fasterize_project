import { Controller, Get, Query } from '@nestjs/common';

import { AppService } from './app.service';
import { urlDto } from './dto/urlDto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getUrl(@Query() query: urlDto): Promise<any> {
    const urlData = await this.appService.getUrlData(query.url);
    const { headersData, status: statusCode } = urlData;

    const edgeLocation = await this.appService.getEdgeLocation();

    const cloudFrontPop = this.appService.getcloudFrontPop(
      headersData['x-amz-cf-pop'],
      edgeLocation,
    );
    const fstrzFlags = this.appService.getFstrzFlags(headersData['x-fstrz']);
    return headersData.server === 'fasterize'
      ? {
          plugged: true,
          cloudfrontPOP: cloudFrontPop.city,
          cloudfrontStatus: headersData['x-cache'],
          statusCode,
          fstrzFlags,
        }
      : { plugged: false, statusCode };
  }
}
