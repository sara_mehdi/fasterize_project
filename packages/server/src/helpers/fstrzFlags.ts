import { Flags } from 'src/dto/flags';

export const fstrzFlags: Flags = {
  h:
    "contenu qui n'est pas du HTML (JSON envoyé avec un content type text/html par exemple)",
  o: 'optimisée',
  '!o': 'non optimisée',
  c: 'cachée',
  '!c': 'non cachée',
  'o,c': ['optimisée', 'cachée'],
  'w,c': ["réponse en cours d'optimisation", 'cachée'],
  'dc,o': ['optimisée', 'cache dynamique'],
  sc: 'la page est cachée via le SmartCache (Cache + Ajax)',
  clc: 'la page est cachée via le Cookie Less Cache',
  ed: 'moteur désactivé',
  v: "objet virtuel (résultat d'une concaténation ou d'un outlining)",
  vf: 'objet virtuel reconstitué',
  t:
    "timeout, l'optimisation a pris trop de temps, la page renvoyée n'est pas optimisée",
  tb: 'too big, la ressource est trop lourde pour être optimisée',
  e: "error, l'optimisation a échoué, la page renvoyée n'est pas optimisée",
  b: "blocked, l'adresse IP est bloquée",
  uc: "User Canceled, la requête a été annulée par l'utilisateur",
  cbo:
    "Circuit Breaker Open, l'objet n'est pas optimisé suite à l'indisponibilité temporaire d'un composant du moteur",
  ccb:
    "Cache CallBack, l'objet est servi par le proxy depuis le cache (cas d'un objet qui n'a pas encore été inclus dans un test A/B)",
  of:
    'Overflow, indique que le système de débordement est en place. Les flags suivants précisent le résultat.',
};
