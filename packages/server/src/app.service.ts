import { Injectable, HttpService } from '@nestjs/common';
import { AmzCode } from './dto/amzCode';
import { fstrzFlags } from './helpers/fstrzFlags';
import { Flags } from './dto/flags';

@Injectable()
export class AppService {
  constructor(private http: HttpService) {}

  async getUrlData(url: string): Promise<any> {
    const observer = await this.http.get(url);
    const response = await observer.toPromise();
    const headersData = response.headers;
    return { headersData, status: response.status };
  }

  async getEdgeLocation(): Promise<any> {
    const observer = await this.http.get(
      'https://cdn.jsdelivr.net/gh/ft6/cloud.feitsui.com/cloudfront-edge-locations.json',
    );
    const response = await observer.toPromise();
    return response.data.nodes;
  }

  getcloudFrontPop(xAmzCfPop: string, edgeLocation: AmzCode[]): AmzCode {
    if (!xAmzCfPop) return undefined;

    const codeNode: string = xAmzCfPop.substr(0, 3);
    const cloudFrontPops: AmzCode = edgeLocation[codeNode];
    return cloudFrontPops;
  }

  getFstrzFlags(flags: string): Flags[] {
    if (!flags) return undefined;

    let data = fstrzFlags[flags];

    if (!data) {
      const fstrzFlag: string[] = flags.split(',');
      data = fstrzFlag.map(flag => {
        return fstrzFlags[flag];
      });
    }

    return data;
  }
}
