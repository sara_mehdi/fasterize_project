export class Flags {
  h: string;
  o: string;
  '!o': string;
  c: string;
  '!c': string;
  'o,c': string[];
  'w,c': string[];
  'dc,o': string[];
  sc: string;
  clc: string;
  ed: string;
  v: string;
  vf: string;
  t: string;
  tb: string;
  e: string;
  b: string;
  uc: string;
  cbo: string;
  ccb: string;
  of: string;
}
