import { IsUrl } from 'class-validator';

export class urlDto {
  @IsUrl({
    require_protocol: true,
    require_valid_protocol: true,
    allow_underscores: true,
  })
  public url: string;
}
