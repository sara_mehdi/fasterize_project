import * as request from 'supertest';
import { HttpStatus } from '@nestjs/common';

const app = 'http://localhost:3001';

describe('Tests on root (/)', () => {
  it('should return fasterize headers ', () => {
    return request(app)
      .get('?url=http://nocibe.fr')
      .set('Accept', 'application/json')
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        const { plugged, statusCode, fstrzFlags, cloudfrontStatus } = body;
        expect(plugged).toEqual(true);
        expect(statusCode).toEqual(200);
        expect(fstrzFlags).toEqual(['optimisée', 'cache dynamique']);
        expect(cloudfrontStatus).toEqual('Miss from cloudfront');
      });
  });

  it('should return plugged false', () => {
    return request(app)
      .get('?url=https://youtube.fr')
      .set('Accept', 'application/json')
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        const {
          plugged,
          statusCode,
          fstrzFlags,
          cloudfrontStatus,
          cloudfrontPOP,
        } = body;
        expect(plugged).toEqual(false);
        expect(statusCode).toEqual(200);
        expect(fstrzFlags).toBeUndefined();
        expect(cloudfrontStatus).toBeUndefined();
        expect(cloudfrontPOP).toBeUndefined();
      });
  });

  it('should return bad request error for empty URL ', () => {
    return request(app)
      .get('?url=')
      .set('Accept', 'application/json')
      .expect(HttpStatus.BAD_REQUEST)
      .expect(({ body }) => {
        const { message, statusCode, error } = body;
        expect(message).toEqual(['url must be an URL address']);
        expect(statusCode).toEqual(400);
        expect(error).toEqual('Bad Request');
      });
  });

  it('should return bad request error for bad shape URL ', () => {
    return request(app)
      .get('?url=https://fasterize.f')
      .set('Accept', 'application/json')
      .expect(HttpStatus.BAD_REQUEST)
      .expect(({ body }) => {
        const { message, statusCode, error } = body;
        expect(message).toEqual(['url must be an URL address']);
        expect(statusCode).toEqual(400);
        expect(error).toEqual('Bad Request');
      });
  });

  it('should return error for unreachable URL ', () => {
    return request(app)
      .get('?url=https://fasterize.ca')
      .set('Accept', 'application/json')
      .expect(HttpStatus.INTERNAL_SERVER_ERROR)
      .expect(({ body }) => {
        const { message, statusCode } = body;
        expect(message).toEqual('Internal server error');
        expect(statusCode).toEqual(500);
      });
  });
});
